Description
======================
Healthcare providers request to be part of the Availity system.  Using React framework, create a registration user interface so healthcare providers can electronically join Availity.  The following data points should be collected:
•	First and Last Name
•	NPI number
•	Business Address
•	Telephone Number
•	Email address

Dependencies
=======================
Node.js Version v16.13.0 or higher

Examples
============================
cd registration-form
npm start
