
define('Registration Form', () => {

    beforeEach(()=> cy.visit('http://localhost:3000/'))
    
    it('Should successfully submit 2 registration forms', () => {
        
        cy.get('[data-cy="firstName"]').type('Hello')
        cy.get('[data-cy="lastName"]').type('World')
        cy.get('[data-cy="npiNumber"]').type('123456')
        cy.get('[data-cy="npiNumberConfirmation"]').type('123456')
        cy.get('[data-cy="businessAddress"]').type('12345 Sesame St.')
        cy.get('[data-cy="businessCity"]').type('Brooklyn')
        cy.get('[data-cy="businessState"]').type('NY')
        cy.get('[data-cy="businessZip"]').type('11111')
        cy.get('[data-cy="phone"]').type('(123)456-7890')
        cy.get('[data-cy="email"]').type('test.me@helloworld.com')
        cy.get('[data-cy="submitForm"]').click()
        
        cy.get('[data-cy="firstName"]').type('Hello 2')
        cy.get('[data-cy="lastName"]').type('World 2')
        cy.get('[data-cy="npiNumber"]').type('1234567')
        cy.get('[data-cy="npiNumberConfirmation"]').type('1234567')
        cy.get('[data-cy="businessAddress"]').type('12345 Sesame St.')
        cy.get('[data-cy="businessCity"]').type('Brooklyn')
        cy.get('[data-cy="businessState"]').type('NY')
        cy.get('[data-cy="businessZip"]').type('11111')
        cy.get('[data-cy="phone"]').type('(123)456-7890')
        cy.get('[data-cy="email"]').type('test.me@helloworld2.com')
        cy.get('[data-cy="submitForm"]').click()

        assert.isOk('everything', 'everything is ok')
    })
    
    it('Should fail to submit registration form due to invalid phone number', () => {
        
        cy.get('[data-cy="firstName"]').type('Hello')
        cy.get('[data-cy="lastName"]').type('World')
        cy.get('[data-cy="npiNumber"]').type('123456')
        cy.get('[data-cy="npiNumberConfirmation"]').type('123456')
        cy.get('[data-cy="businessAddress"]').type('12345 Sesame St.')
        cy.get('[data-cy="businessCity"]').type('Brooklyn')
        cy.get('[data-cy="businessState"]').type('NY')
        cy.get('[data-cy="businessZip"]').type('11111')
        cy.get('[data-cy="phone"]').type('(123)qqq-7890')
        cy.get('[data-cy="email"]').type('test.me@helloworld.com')
        cy.get('[data-cy="submitForm"]').click()

        assert.isOk('everything', 'everything is ok')
    })
})