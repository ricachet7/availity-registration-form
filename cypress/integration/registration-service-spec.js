
import { RegistrationService } from '../../registration-form/src/lib/js/services/registration-service'

define('RegistrationService', () => {

    it('submit should add new registration to list of submitted registrations when valid regirstion is submitted', () => {

        const registrationService = new RegistrationService()
        const registration = {
            firstName: 'hello',
            lastName: 'world',
            npiNumber: '1234',
            confirmationNPINumber: '1234',
            businessAddress: '12345 Sesame St.',
            businessCity: 'asdf',
            businessState: 'asdf',
            businessZip: 'adf',
            telephoneNumber: '(123)456-7890',
            emailAddress: 'test.me@helloworld.com'
        }
        const submittedRegirstion = registrationService.submit(registration)

        expect(submittedRegirstion.id).to.be.above(0)
    })

    it('submit should reject new registration when regirstion is submitted with missing required field', () => {

        const registrationService = new RegistrationService()
        const registration = {
            firstName: 'hello',
            lastName: 'world',
            npiNumber: '1234',
            confirmationNPINumber: '1234',
            businessAddress: '12345 Sesame St.',
            businessCity: 'asdf',
            businessState: 'asdf',
            businessZip: 'adf',
            telephoneNumber: '(123)456-7890',
            emailAddress: null
        }
        const submittedRegirstion = registrationService.submit(registration)

        expect(submittedRegirstion.id).to.be.equal(undefined)
        expect(submittedRegirstion.messages.length).to.be.above(0)
    })

    it('submit should reject new registration when regirstion is submitted with invalid email address format', () => {

        const registrationService = new RegistrationService()
        const registration = {
            firstName: 'hello',
            lastName: 'world',
            npiNumber: '1234',
            confirmationNPINumber: '1234',
            businessAddress: '12345 Sesame St.',
            businessCity: 'asdf',
            businessState: 'asdf',
            businessZip: 'adf',
            telephoneNumber: '(123)456-7890',
            emailAddress: 'test.mehelloworld.com'
        }
        const submittedRegirstion = registrationService.submit(registration)

        expect(submittedRegirstion.id).to.be.equal(undefined)
        expect(submittedRegirstion.messages.length).to.be.above(0)
    })

    it('submit should reject new registration when regirstion is submitted with invalid telephone number format', () => {

        const registrationService = new RegistrationService()
        const registration = {
            firstName: 'hello',
            lastName: 'world',
            npiNumber: '1234',
            confirmationNPINumber: '1234',
            businessAddress: '12345 Sesame St.',
            businessCity: 'asdf',
            businessState: 'asdf',
            businessZip: 'adf',
            telephoneNumber: '(123)qqq-7890',
            emailAddress: 'test.me@helloworld.com'
        }
        const submittedRegirstion = registrationService.submit(registration)

        expect(submittedRegirstion.id).to.be.equal(undefined)
        expect(submittedRegirstion.messages.length).to.be.above(0)
    })
})