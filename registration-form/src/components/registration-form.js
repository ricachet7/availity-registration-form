const RegistrationForm = ({ firstName, lastName, npiNumber, confirmationNPINumber, businessAddress, businessCity, 
    businessState, businessZip, telephoneNumber, emailAddress, messages , onFormSubmission, successFullySubmittedFormId }) => {

    const submitForm = (event) => {
        event.preventDefault();

        const newRegistration = {
            firstName: _firstName,
            lastName: _lastName,
            npiNumber: _npiNumber,
            confirmationNPINumber: _ConfirmationNPINumber,
            businessAddress: _businessAddress,
            businessCity: _businessCity,
            businessState: _businessState,
            businessZip: _businessZip,
            telephoneNumber: _telephoneNumber,
            emailAddress: _emailAddress
          }

        if(onFormSubmission(newRegistration))
            document.getElementById('registration-form').reset();
    }

    let _firstName = firstName;
    let _lastName = lastName;
    let _npiNumber = npiNumber;
    let _ConfirmationNPINumber = confirmationNPINumber;
    let _businessAddress = businessAddress;
    let _businessCity = businessCity;
    let _businessState = businessState;
    let _businessZip = businessZip;
    let _telephoneNumber = telephoneNumber;
    let _emailAddress = emailAddress;

    return (
        <div className="registration-form-container">
            {
                messages.length > 0
                ? <div className="alert alert-danger" role="alert">
                    <ul>
                        {
                            messages.map((m, i) => (<li key={i}>{ m }</li>))
                        }
                    </ul>
                </div>
                : null
            }
            {
                successFullySubmittedFormId > 0
                ? <div className="alert alert-success" role="alert">{`Registration successful: Confirmation #${successFullySubmittedFormId}`}</div>
                : null
            }
            <form id="registration-form">
                <div className="input-group mb-3">
                    <label className="form-label" htmlFor="firstName">First Name</label>
                    <input data-cy="firstName"
                        ref={ () => _firstName }
                        onChange={ (e) => _firstName = e.target.value}
                        type="text" className="form-control" id="firstName" placeholder="Enter first name" maxLength="30" />
                </div>
                <div className="input-group mb-3">
                    <label className="form-label" htmlFor="lastName">Last Name</label>
                    <input data-cy="lastName"
                        ref={ () => _lastName }
                        onChange={ (e) => _lastName = e.target.value}
                        type="text" className="form-control" id="lastName" placeholder="Enter last name" maxLength="30" />
                </div>
                <div className="input-group mb-3">
                    <label className="form-label" htmlFor="npiNumber">NPI Number</label>
                    <input data-cy="npiNumber"
                        ref={ () => _npiNumber }
                        onChange={ (e) => _npiNumber = e.target.value}
                        type="password" className="form-control" id="npiNumber" placeholder="Enter NPI number" maxLength="30" />
                </div>
                <div className="input-group mb-3">
                    <label className="form-label" htmlFor="npiNumberConfirmation">Re-Enter NPI Number</label>
                    <input data-cy="npiNumberConfirmation"
                        ref={ () => _ConfirmationNPINumber }
                        onChange={ (e) => _ConfirmationNPINumber = e.target.value}
                        type="password" className="form-control" id="npiNumberConfirmation" placeholder="Re-enter NPI number" />
                </div>
                <div className="input-group mb-3">
                    <label className="form-label" htmlFor="businessAddress">Business Address</label>
                    <input data-cy="businessAddress"
                        ref={ () => _businessAddress }
                        onChange={ (e) => _businessAddress = e.target.value}
                        type="text" className="form-control" id="businessAddress" placeholder="Enter business address" maxLength="100" />
                </div>
                <div className="input-group mb-3">
                    <label className="form-label" htmlFor="businessCity">Business City</label>
                    <input data-cy="businessCity"
                        ref={ () => _businessCity }
                        onChange={ (e) => _businessCity = e.target.value}
                        type="text" className="form-control" id="businessCity" placeholder="Enter business city" maxLength="80" />
                </div>
                <div className="input-group mb-3">
                    <label className="form-label" htmlFor="businessState">Business State</label>
                    <input data-cy="businessState"
                        ref={ () => _businessState }
                        onChange={ (e) => _businessState = e.target.value}
                        type="text" className="form-control" id="businessState" placeholder="Enter business state" maxLength="2" />
                </div>
                <div className="input-group mb-3">
                    <label className="form-label" htmlFor="businessZip">Business Zip</label>
                    <input data-cy="businessZip"
                        ref={ () => _businessZip }
                        onChange={ (e) => _businessZip = e.target.value}
                        type="text" className="form-control" id="businessZip" placeholder="Enter business zip" maxLength="5" />
                </div>
                <div className="input-group mb-3">
                    <label className="form-label" htmlFor="phone">Telephone Number</label>
                    <input data-cy="phone"
                        ref={ () => _telephoneNumber }
                        onChange={ (e) => _telephoneNumber = e.target.value}
                        type="phone" className="form-control" id="phone" placeholder="Enter telephone number" maxLength="13" />
                </div>
                <div className="input-group mb-3">
                    <label className="form-label" htmlFor="email">Email Address</label>
                    <input data-cy="email"
                        ref={ () => _emailAddress }
                        onChange={ (e) => _emailAddress = e.target.value}
                        type="email" className="form-control" id="email" placeholder="Enter email address" maxLength="250" />
                </div>
                <div className="pt-3">
                    <button data-cy="submitForm" id="submitForm" type="button" className="btn btn-primary" onClick={ submitForm }>Submit</button>
                </div>
            </form>
        </div>
        )
}

export default RegistrationForm