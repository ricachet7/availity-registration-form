import { Component } from 'react';
import ReactDOM from 'react-dom';
import './App.css';
import { RegistrationService } from './lib/js/services/registration-service'
import RegistrationForm from './components/registration-form'

class App extends Component {

  _registrationService;
  
  constructor(props){
    super(props)

    this._registrationService = new RegistrationService()

    this.state = {
        successFullySubmittedFormId: 0,
        submittedRegistrations: [...this._registrationService.list()], 
        registrationForm: {
          firstName: '',
          lastName: '',
          npiNumber: '',
          confirmationNPINumber: '',
          businessAddress: '',
          businessCity: '',
          businessState: '',
          businessZip: '',
          telephoneNumber: '',
          emailAddress: '',
          messages: []
        }
      }

      this.handleFormSubmission = this.handleFormSubmission.bind(this);
  }

  handleFormSubmission(newRegisration){
    
    const submissionResult = this._registrationService.submit(newRegisration);
    const success = submissionResult.messages.length === 0;
    const successFullySubmittedFormId = success ? submissionResult.id : 0;
    const registrationForm = success 
      ? {
          firstName: '',
          lastName: '',
          npiNumber: '',
          confirmationNPINumber: '',
          businessAddress: '',
          businessCity: '',
          businessState: '',
          businessZip: '',
          telephoneNumber: '',
          emailAddress: '',
          messages: []
      }
      : { ...submissionResult }
      
      this.setState({
        successFullySubmittedFormId,
        submittedRegistrations: [...this._registrationService.list()], 
        registrationForm
      });

      return success;
  }

  render (){
    return (<RegistrationForm 
      firstName={ this.state.registrationForm.firstName } 
      lastName={ this.state.registrationForm.lastName } 
      npiNumber={ this.state.registrationForm.npiNumber }  
      confirmationNPINumber={ this.state.registrationForm.confirmationNPINumber }
      businessAddress={ this.state.registrationForm.businessAddress } 
      businessCity={ this.state.registrationForm.businessCity }
      businessState={ this.state.registrationForm.businessState }
      businessZip={ this.state.registrationForm.businessZip }
      telephoneNumber={ this.state.registrationForm.telephoneNumber } 
      emailAddress={ this.state.registrationForm.emailAddress } 
      messages={ this.state.registrationForm.messages }
      successFullySubmittedFormId={ this.state.successFullySubmittedFormId }
      onFormSubmission={ this.handleFormSubmission } />)
  }
}

export default App;
