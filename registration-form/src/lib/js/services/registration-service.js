var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var validateRequried = function (key, val) {
    if (['id', 'submissiondate', 'messages'].includes(key.toLowerCase()))
        return true;
    return val != null && val != undefined && val.trim() != '';
};
var validatePhone = function (val) {
    var data = val.trim().replace(/\W/gi, '');
    return data.match(/\d{10}/) != null && val.match(/[^\d()-]+/) === null;
};
var validateEmail = function (val) { return val.trim().match(/\S+@\S+\.\S+/) != null; };
var validateNPINumber = function (npiNumber, confirmationNPINumber) { return npiNumber === confirmationNPINumber; };
var validate = function (registration) {
    var messages = [];
    var reg = registration;
    var valid = Object.keys(registration).reduce(function (stillValid, key) {
        if (!validateRequried(key, reg[key]))
            messages.push("Required field: " + formatPropFormErrorMessage(key));
        return messages.length === 0;
    }, true);
    if (valid) {
        valid = validatePhone(registration.telephoneNumber);
        if (!valid)
            messages.push('Invalid telephone number format');
    }
    if (valid) {
        valid = validateEmail(registration.emailAddress);
        if (!valid)
            messages.push('Invalid email address format');
    }
    if (valid) {
        valid = validateNPINumber(registration.npiNumber, registration.confirmationNPINumber);
        if (!valid)
            messages.push('Re-entered NPI number does not match NPI number');
    }
    return messages;
};
var formatPropFormErrorMessage = function (propName) {
    switch (propName.toLowerCase()) {
        case 'firstname':
            return 'First Name';
        case 'lastname':
            return 'Last Name';
        case 'npinumber':
            return 'NPI Number';
        case 'confirmationnpinumber':
            return 'Re-Enter NPI Number';
        case 'businessaddress':
            return 'Business Address';
        case 'businesscity':
            return 'Business City';
        case 'businessstate':
            return 'Business State';
        case 'businesszip':
            return 'Business Zip';
        case 'telephonenumber':
            return 'Telephone Number';
        default:
            return 'Email Address';
    }
};
var _USERNAME = 'hello.world@gmail.com';
var _PASSWORD = 'asdf';
var RegistrationService = (function () {
    function RegistrationService() {
        var _this = this;
        this.list = function () { return _this._registrations; };
        this.login = function (username, password) { return username === _USERNAME && password === _PASSWORD; };
        this._registrations = [];
    }
    RegistrationService.prototype.submit = function (newRegistration) {
        var cp = __assign({}, newRegistration);
        cp.messages = validate(cp);
        if (cp.messages.length === 0) {
            cp.id = this._registrations.length + 1;
            cp.submissionDate = new Date();
            this._registrations.push(cp);
        }
        return cp;
    };
    return RegistrationService;
}());
export { RegistrationService };
//# sourceMappingURL=registration-service.js.map