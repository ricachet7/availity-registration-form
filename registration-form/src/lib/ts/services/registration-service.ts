import { IRegistration } from "../models/registration";

const validateRequried = (key: string, val: string): boolean => {

    if(['id','submissiondate', 'messages'].includes(key.toLowerCase())) return true;

    return val != null && val != undefined && val.trim() != ''
}
const validatePhone = (val: string): boolean => {

    const data = val.trim().replace(/\W/gi, '');

    return data.match(/\d{10}/) != null && val.match(/[^\d()-]+/) === null;
}
const validateEmail = (val: string): boolean => val.trim().match(/\S+@\S+\.\S+/) != null
const validateNPINumber = (npiNumber: string, confirmationNPINumber: string): boolean => npiNumber === confirmationNPINumber
const validate = (registration: IRegistration): string[] => {
    
    let messages: string[] = [];
    const reg: any = registration;
    let valid = Object.keys(registration).reduce((stillValid: boolean, key) =>{

        if(!validateRequried(key, reg[key]))
            messages.push(`Required field: ${formatPropFormErrorMessage(key)}`)

        return messages.length === 0;

    }, true)

    if(valid){
        valid = validatePhone(registration.telephoneNumber);

        if(!valid)
            messages.push('Invalid telephone number format')
    }
    
    if(valid){
        valid = validateEmail(registration.emailAddress);

        if(!valid)
            messages.push('Invalid email address format')
    }
    
    if(valid){
        valid = validateNPINumber(registration.npiNumber, registration.confirmationNPINumber);

        if(!valid)
            messages.push('Re-entered NPI number does not match NPI number')
    }

    return messages;
}

const formatPropFormErrorMessage = (propName: string): string => {

    switch(propName.toLowerCase()){
        case 'firstname':
            return 'First Name'
        case 'lastname':
            return 'Last Name'
        case 'npinumber':
            return 'NPI Number'
        case 'confirmationnpinumber':
            return 'Re-Enter NPI Number'
        case 'businessaddress':
            return 'Business Address'
        case 'businesscity':
            return 'Business City'
        case 'businessstate':
            return 'Business State'
        case 'businesszip':
            return 'Business Zip'
        case 'telephonenumber':
            return 'Telephone Number'
        default:
            return 'Email Address'
    }
}

const _USERNAME: string = 'hello.world@gmail.com';
const _PASSWORD: string = 'asdf';

export class RegistrationService {

    private _registrations: IRegistration[];

    constructor(){
        this._registrations = [];
    }

    public submit(newRegistration: IRegistration): IRegistration {

        const cp: IRegistration = { ...newRegistration };

        cp.messages = validate(cp);

        if(cp.messages.length === 0){
            cp.id = this._registrations.length + 1;
            cp.submissionDate = new Date();
            this._registrations.push(cp);
        }

        return cp;
    }

    public list = (): IRegistration[] => this._registrations;
    public login = (username: string, password: string) => username === _USERNAME && password === _PASSWORD;
}