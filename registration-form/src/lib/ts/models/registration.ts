export interface IRegistration {
    id: number
    firstName:string
    lastName: string
    npiNumber: string
    confirmationNPINumber: string
    businessAddress: string
    businessCity: string
    businessState: string
    businessZip: string
    telephoneNumber: string
    emailAddress: string
    messages: string[]
    submissionDate: Date
}